#!bin/sh
echo '10.18.4.10 redis.mall'   >> /etc/hosts
echo '10.18.4.10 mysql.mall'  >> /etc/hosts
echo '10.18.4.10 kafka.mall'  >> /etc/hosts
echo '10.18.4.10 zookeeper.mall'  >> /etc/hosts


sleep 10
nohup java -jar user-provider-0.0.1-SNAPSHOT.jar &
sleep 10
nohup java -jar shopping-provider-0.0.1-SNAPSHOT.jar &
sleep 10
nohup java -jar gpmall-user-0.0.1-SNAPSHOT.jar &
sleep 10
nohup java -jar gpmall-shopping-0.0.1-SNAPSHOT.jar &
