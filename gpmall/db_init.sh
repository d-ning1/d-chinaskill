#!/bin/bash
mysql_install_db --user=mysql
sleep 3
mysqld_safe &
sleep 3
mysqladmin -u "$MYSQL_ROOT" password "$MYSQL_ROOT_PASSWORD"


mysql  -u"$MYSQL_ROOT" -p"$MYSQL_ROOT_PASSWORD" -e "create database gpmall"

mysql  -u"$MYSQL_ROOT" -p"$MYSQL_ROOT_PASSWORD"  gpmall -e  "source ./gpmall.sql"


mysql  -u"$MYSQL_ROOT" -p"$MYSQL_ROOT_PASSWORD"  -e "grant all privileges on *.* to '$MYSQL_ROOT'@'%' identified by '$MYSQL_ROOT_PASSWORD';"

mysql  -u"$MYSQL_ROOT" -p"$MYSQL_ROOT_PASSWORD"  -e "grant all privileges on *.* to 'gpmall'@'%' identified by '$MYSQL_ROOT_PASSWORD';"

mysql -u"$MYSQL_ROOT" -p"$MYSQL_ROOT_PASSWORD" -e "SET character_set_client = utf8;SET character_set_connection = utf8;"

mysql -u"$MYSQL_ROOT" -p"$MYSQL_ROOT_PASSWORD" -e "SET character_set_database = utf8;SET character_set_results = utf8;SET character_set_server = utf8;"

mysql  -u"$MYSQL_ROOT" -p"$MYSQL_ROOT_PASSWORD"  -e "flush privileges;"


