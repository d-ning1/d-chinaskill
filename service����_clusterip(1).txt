cat service-clusterip.yaml 
apiVersion: v1
kind: Service
metadata:
  labels:
    run: nginx
  name: service-clusterip
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 81
  selector:
    app: nginx
  type: ClusterIP