环境准备
1.规划节点
Ansible服务的节点规划，见表1-1-1。

表1-1-1规划节点

IP	主机名	节点
192.168.100.20	ansible	Ansible节点
192.168.100.21	node1	Node1节点
192.168.100.22	node2	Node2节点
192.168.100.23	node3	Node3节点
2.基础准备
使用OpenStack平台创建两台云主机进行实验，云主机镜像使用提供的CentOS_7.5_x86_64_XD.qcow2镜像，flavor使用1核/2G内存/20G硬盘，自行配置网络并使用远程连接工具连接云主机。节点规划表中的IP地址为作者的IP地址，在进行实操案例的时候，按照自己的环境规划网络与IP地址。Ansible节点安装好Ansible服务。然后做好Ansible节点对所有node节点的无秘钥访问操作.

相关软件包下载

[ypbtn]https://pan.baidu.com/s/1PSyBPXwpQfpkUgDoMUKDKw[/ypbtn]

提取码：rqil

配置主机名和hosts

主节点安装ansible服务

yum -y install ansible

配置无秘钥登录

ssh-keygen

ssh-copy-id node1（2和3也要）

创建工作目录

[root@ansible ~]# mkdir /root/example

创建角色

mkdir -p /root/example/roles/{init,mariadb}/{tasks,files,templates,meta,handlers,vars}

创建group_vars目录

cd /root/example/
mkdir group_vars
touch group_vars/all

创建安装入口文件（example目录下）

touch cscc_install.yaml

3.编写Playbook剧本
(1)init角色
该角色执行的任务是用来部署三个node节点的基础环境，包括关闭防火墙，SELinux，配置yum源，在roles/init/tasks目录下，创建main.yaml文件，文件的内容如下：

cd example/roles/init/tasks

vi main.yaml

- name: Selinux Config Setenforce
  shell: getenforce
  register: info
- name: when_Selinux
  shell: setenforce 0
  when: info['stdout'] == 'Enforcing'
- name: rm repos
  shell: rm -rf /etc/yum.repos.d/*
- name: create ftp.repo
  copy: src=ftp.repo dest=/etc/yum.repos.d/
- name: copy hosts
  template: src=hosts.j2 dest=/etc/hosts
在该init角色剧本中，即把ftp.repo文件拷贝至init/files目录下，把hosts.j2文件拷贝至init/templates目录下。下面贴出ftp.repo和host.j2的文件内容：

ftp.repo文件的配置

[centos]
name=centos
baseurl=ftp://192.168.100.100/centos
gpgcheck=0
enabled=1
[mariadb]
name=mariadb
baseurl=ftp://192.168.100.100/mariadb-repo
gpgcheck=0
enabled=1
hosts.j2文件的配置

127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
{{hostip1}} {{hostname1}}
{{hostip2}} {{hostname2}}
{{hostip3}} {{hostname3}}
因为设置的变量，所以需要在/root/example/group_vars/all中声明变量，all文件内容如下：

hostip1: 192.168.100.21
hostip2: 192.168.100.22
hostip3: 192.168.100.23
hostname1: node1
hostname2: node2
hostname3: node3
DB_PASS: 123456
至此，init角色剧本编写完成。

(2)Mariadb角色
该角色的作用是在三个节点上安装数据库服务并配置成数据库MariaDB Galera Cluster集群，在roles/mariadb/tasks目录下，创建main.yaml文件，文件的内容如下：

cd example/roles/mariadb/tasks

vi main.yaml

- name: install mariadb
  yum: name=mariadb-server state=present
- name: install mariadb-galera
  yum: name=galera state=present
- name: start mariadb
  shell: systemctl start mariadb
- name: enable mariadb
  shell: systemctl enable mariadb
- name: install expect 
  yum: name=expect state=present
- name: mysql_secure_installation
  template: src=mysql_secure_installation.sh.j2 dest=/opt/mysql_secure_installation.sh
- name: sh shell
  shell: sh /opt/mysql_secure_installation.sh
- name: config mariadb
  template: src=server.cnf.j2 dest=/etc/my.cnf.d/server.cnf
- name: grant privileges
  shell: mysql -uroot -p{{ DB_PASS }} -e "grant all privileges on *.* to 'root'@'%' identified by '{{ DB_PASS }}';"
- name: stop db
  shell: systemctl stop mariadb
- name: new cluster
  shell: galera_new_cluster
  when: ansible_fqdn=="node1"
- name: restart db
  shell: systemctl restart mariadb
  when: ansible_fqdn=="node2"
- name: restart db
  shell: systemctl restart mariadb
  when: ansible_fqdn=="node3"
在该mariadb角色剧本中，用到了template模块，需要把server.cnf.j2和mysql_secure_installation.sh.j2文件放入tasks同级别的templates目录下，下面贴出两个j2文件内容：

server.cnf.j2

server.cnf.j2文件为数据库的配置文件，因需要确保各个节点的配置，需要使用变量。

[server]
# this is only for the mysqld standalone daemon
[mysqld]
#
# * Galera-related settings
#
[galera]
wsrep_on=ON
wsrep_provider=/usr/lib64/galera/libgalera_smm.so
wsrep_cluster_address="gcomm://{{hostip1}},{{hostip2}},{{hostip3}}"
{% if ansible_fqdn == "node1" %}
wsrep_node_name= {{hostname1}}
wsrep_node_address={{hostip1}}
{% elif ansible_fqdn == "node2" %}
wsrep_node_name= {{hostname2}}
wsrep_node_address={{hostip2}}
{% else %}
wsrep_node_name= {{hostname3}}
wsrep_node_address={{hostip3}}
{% endif %}
binlog_format=row
default_storage_engine=InnoDB
innodb_autoinc_lock_mode=2
wsrep_slave_threads=1
innodb_flush_log_at_trx_commit=0
innodb_buffer_pool_size=120M
wsrep_sst_method=rsync
wsrep_causal_reads=ON
#
# Allow server to accept connections on all interfaces.
#
{% if ansible_fqdn == "node1" %}
bind-address={{hostip1}}
{% elif ansible_fqdn == "node2" %}
bind-address={{hostip2}}
{% else %}
bind-address={{hostip3}}
{% endif %}
mysql_secure_installation.sh.j2

mysql_secure_installation.sh.j2文件为一个shell脚本，用来初始化数据库

#!/bin/bash
#检查是否是root用户执行
expect -c "
spawn /usr/bin/mysql_secure_installation
expect \"Enter current password for root (enter for none):\"
send \"\r\"
expect \"Set root password?\"
send \"y\r\"
expect \"New password:\"
send \"{{DB_PASS}}\r\"
expect \"Re-enter new password:\"
send \"{{DB_PASS}}\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"n\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof"
4.执行Playbook
（1）修改hosts文件
vi  /etc/ansible/hosts

[hosts]
node1
node2
node3
（2）编辑剧本入口文件
cscc_install.yaml文件为执行剧本的入口文件，需要将调用roles的顺序及哪些主机调用哪些roles在这个文件中体现出来，cscc_install.yaml文件的具体内容如下：

---
- hosts: hosts
  remote_user: root
 
  roles: 
    - init
    - mariadb
（3）执行剧本
当所有准备工作都完成之后，使用ansible-playbook命令执行剧本，首先使用--syntax-check参数检测脚本的语法，命令如下：

[root@ansible example]# ansible-playbook cscc_install.yaml --syntax-check
playbook: cscc_install.yaml
直接返回文件名，表示脚本没有语法错误。执行剧本，命令如下：

[root@ansible example]# ansible-playbook cscc_install.yaml
在等待一段时间之后，剧本执行完毕，若没有报错，高可用数据库集群部署完毕



至此，使用Ansible一键部署高可用数据库